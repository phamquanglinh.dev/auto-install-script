@echo off

:: Kiểm tra xem Laragon đã được cài đặt
if not exist "C:\laragon\laragon.exe" (
  :: Tải Laragon từ trang web chính thức
  echo Downloading Laragon...
  powershell -Command "Invoke-WebRequest -Uri 'https://github.com/leokhoa/laragon/releases/download/6.0.0/laragon-wamp.exe' -OutFile '%TEMP%\laragon.exe'"

  :: Cài đặt Laragon
  echo Installing Laragon...
  start /wait "" "%TEMP%\laragon.exe" /S
  if exist "C:\laragon\laragon.exe" (
    echo Laragon installation completed.
  ) else (
    echo Laragon installation failed.
    exit /b 1
  )
) else (
  echo Laragon is already installed.
)

:: Kiểm tra xem Composer đã được cài đặt
where composer 2>nul
if %errorlevel% neq 0 (
  :: Tải Composer
  echo Downloading Composer...
  powershell -Command "Invoke-WebRequest -Uri 'https://getcomposer.org/Composer-Setup.exe' -OutFile '%TEMP%\Composer-Setup.exe'"

  :: Cài đặt Composer
  echo Installing Composer...
  start /wait "" "%TEMP%\Composer-Setup.exe" /S
  if exist "C:\ProgramData\ComposerSetup\bin\composer.bat" (
    echo Composer installation completed.
  ) else (
    echo Composer installation failed.
    exit /b 1
  )
) else (
  echo Composer is already installed.
)

:: Cài đặt Chocolatey (nếu chưa có)
if not exist "%SystemRoot%\System32\chocolatey\choco.exe" (
  echo Installing Chocolatey...
  @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
)

:: Kiểm tra xem Chocolatey đã được cài đặt thành công hay chưa
choco --version 2>nul
if %errorlevel% neq 0 (
  echo Chocolatey installation failed. Exiting...
  pause
  exit /b 1
)

:: Cài đặt các ứng dụng khác bằng Chocolatey
:: Ví dụ: Cài đặt PHPStorm, Node.js, Git, Postman, và các ứng dụng khác.
:: Thay đổi các lệnh choco install cho các ứng dụng bạn muốn cài đặt.

:: Cài đặt PHPStorm bằng Chocolatey
echo Installing PHPStorm...
choco install phpstorm -y
cls

:: Cài đặt Node.js bằng Chocolatey
echo Installing Node.js...
choco install nodejs -y
cls

:: Cài đặt Git bằng Chocolatey
echo Installing Git...
choco install git -y
cls

:: Cài đặt Postman bằng Chocolatey
echo Installing Postman...
choco install postman -y
cls

:: Hiển thị thông báo hoàn thành
echo Installation completed.

:: Dừng để giữ cửa sổ console mở
pause
